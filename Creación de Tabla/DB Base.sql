/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     17/11/2021 12:33:24 a. m.                    */
/*==============================================================*/

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   ID_CLIENTE           INT4                 not null,
   DATE_CLIENTE         DATE                 null,
   GENERO_CLIENTE       TEXT                 null,
   NOMBRE_CLIENTE       TEXT                 null,
   APELLIDO_CLIENTE     TEXT                 null,
   DIRECCION_CLIENTE    TEXT                 null,
   TELEFONO_CLIENTE     NUMERIC              null,
   TELEFONO2_CLIENTE    NUMERIC              null,
   NACIONALIDAD_CLIENTE TEXT                 null,
   constraint PK_CLIENTE primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Index: CLIENTE_PK                                            */
/*==============================================================*/
create unique index CLIENTE_PK on CLIENTE (
ID_CLIENTE
);

/*==============================================================*/
/* Table: CUIDADOR                                              */
/*==============================================================*/
create table CUIDADOR (
   ID_CUIDADOR          INT4                 not null,
   DATE_INGRESO_CUIDADOR DATE                 null,
   PAGO_DIA_CUIDADOR    DECIMAL              null,
   NOMBRE_CUIDADOR      TEXT                 null,
   constraint PK_CUIDADOR primary key (ID_CUIDADOR)
);

/*==============================================================*/
/* Index: CUIDADOR_PK                                           */
/*==============================================================*/
create unique index CUIDADOR_PK on CUIDADOR (
ID_CUIDADOR
);

/*==============================================================*/
/* Table: ESTADO_ENTREGA                                        */
/*==============================================================*/
create table ESTADO_ENTREGA (
   ID_ESTADO_ENTREGA    INT4                 not null,
   NOMBRE_ESTADO        TEXT                 null,
   constraint PK_ESTADO_ENTREGA primary key (ID_ESTADO_ENTREGA)
);

/*==============================================================*/
/* Index: ESTADO_ENTREGA_PK                                     */
/*==============================================================*/
create unique index ESTADO_ENTREGA_PK on ESTADO_ENTREGA (
ID_ESTADO_ENTREGA
);

/*==============================================================*/
/* Table: FORMA_PAGO                                            */
/*==============================================================*/
create table FORMA_PAGO (
   ID_FORMA_PAGO        INT4                 not null,
   NOMBRE_FORMA_PAGO    TEXT                 null,
   constraint PK_FORMA_PAGO primary key (ID_FORMA_PAGO)
);

/*==============================================================*/
/* Index: FORMA_PAGO_PK                                         */
/*==============================================================*/
create unique index FORMA_PAGO_PK on FORMA_PAGO (
ID_FORMA_PAGO
);

/*==============================================================*/
/* Table: MASCOTA                                               */
/*==============================================================*/
create table MASCOTA (
   ID_MASCOTA           INT4                 not null,
   ID_CLIENTE           INT4                 not null,
   DATE_MASCOTA         DATE                 null,
   NOMBRE_MASCOTA       TEXT                 null,
   RAZA_MASCOTA         TEXT                 null,
   ESTADO_MASCOTA       TEXT                 null,
   constraint PK_MASCOTA primary key (ID_MASCOTA)
);

/*==============================================================*/
/* Index: MASCOTA_PK                                            */
/*==============================================================*/
create unique index MASCOTA_PK on MASCOTA (
ID_MASCOTA
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_1_FK on MASCOTA (
ID_CLIENTE
);

/*==============================================================*/
/* Table: MASCOTA_NO_RECLAMADA                                  */
/*==============================================================*/
create table MASCOTA_NO_RECLAMADA (
   ID_MASCOTA_ABANDONADA INT4                 not null,
   ID_SERVICIO          INT4                 null,
   TIPO_PROCESOS        TEXT                 null,
   constraint PK_MASCOTA_NO_RECLAMADA primary key (ID_MASCOTA_ABANDONADA)
);

/*==============================================================*/
/* Index: MASCOTA_NO_RECLAMADA_PK                               */
/*==============================================================*/
create unique index MASCOTA_NO_RECLAMADA_PK on MASCOTA_NO_RECLAMADA (
ID_MASCOTA_ABANDONADA
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on MASCOTA_NO_RECLAMADA (
ID_SERVICIO
);

/*==============================================================*/
/* Table: PAGO                                                  */
/*==============================================================*/
create table PAGO (
   ID_PAGO              INT4                 not null,
   ID_SERVICIO          INT4                 null,
   ID_TIPO_PAGO         INT4                 null,
   ID_FORMA_PAGO        INT4                 null,
   FECHA_PAGO           DATE                 null,
   CANTIDAD_PAGO        DECIMAL              null,
   constraint PK_PAGO primary key (ID_PAGO)
);

/*==============================================================*/
/* Index: PAGO_PK                                               */
/*==============================================================*/
create unique index PAGO_PK on PAGO (
ID_PAGO
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_10_FK on PAGO (
ID_SERVICIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_11_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_11_FK on PAGO (
ID_TIPO_PAGO
);

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_12_FK on PAGO (
ID_FORMA_PAGO
);

/*==============================================================*/
/* Table: SERVICIO                                              */
/*==============================================================*/
create table SERVICIO (
   ID_SERVICIO          INT4                 not null,
   ID_MASCOTA           INT4                 null,
   ID_TIPO_SERVICIO     INT4                 null,
   ID_ESTADO_ENTREGA    INT4                 null,
   ID_CUIDADOR          INT4                 null,
   FECHA_INICIO         DATE                 null,
   FECHA_FIN            DATE                 null,
   N_SEMANA             INT4                 null,
   constraint PK_SERVICIO primary key (ID_SERVICIO)
);

/*==============================================================*/
/* Index: SERVICIO_PK                                           */
/*==============================================================*/
create unique index SERVICIO_PK on SERVICIO (
ID_SERVICIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on SERVICIO (
ID_MASCOTA
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on SERVICIO (
ID_TIPO_SERVICIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on SERVICIO (
ID_ESTADO_ENTREGA
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on SERVICIO (
ID_CUIDADOR
);

/*==============================================================*/
/* Table: TIPO_PAGO                                             */
/*==============================================================*/
create table TIPO_PAGO (
   ID_TIPO_PAGO         INT4                 not null,
   NOMBRE_TIPO_PAGO     TEXT                 null,
   constraint PK_TIPO_PAGO primary key (ID_TIPO_PAGO)
);

/*==============================================================*/
/* Index: TIPO_PAGO_PK                                          */
/*==============================================================*/
create unique index TIPO_PAGO_PK on TIPO_PAGO (
ID_TIPO_PAGO
);

/*==============================================================*/
/* Table: TIPO_SERVICIO                                         */
/*==============================================================*/
create table TIPO_SERVICIO (
   ID_TIPO_SERVICIO     INT4                 not null,
   NOMBRE_TIPO_SERVICIO TEXT                 null,
   COSTO_TIPO_SERVICIO  DECIMAL              null,
   constraint PK_TIPO_SERVICIO primary key (ID_TIPO_SERVICIO)
);

/*==============================================================*/
/* Index: TIPO_SERVICIO_PK                                      */
/*==============================================================*/
create unique index TIPO_SERVICIO_PK on TIPO_SERVICIO (
ID_TIPO_SERVICIO
);

alter table MASCOTA
   add constraint FK_MASCOTA_RELATIONS_CLIENTE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE)
      on delete restrict on update restrict;

alter table MASCOTA_NO_RECLAMADA
   add constraint FK_MASCOTA__RELATIONS_SERVICIO foreign key (ID_SERVICIO)
      references SERVICIO (ID_SERVICIO)
      on delete restrict on update restrict;

alter table PAGO
   add constraint FK_PAGO_RELATIONS_SERVICIO foreign key (ID_SERVICIO)
      references SERVICIO (ID_SERVICIO)
      on delete restrict on update restrict;

alter table PAGO
   add constraint FK_PAGO_RELATIONS_TIPO_PAG foreign key (ID_TIPO_PAGO)
      references TIPO_PAGO (ID_TIPO_PAGO)
      on delete restrict on update restrict;

alter table PAGO
   add constraint FK_PAGO_RELATIONS_FORMA_PA foreign key (ID_FORMA_PAGO)
      references FORMA_PAGO (ID_FORMA_PAGO)
      on delete restrict on update restrict;

alter table SERVICIO
   add constraint FK_SERVICIO_RELATIONS_CUIDADOR foreign key (ID_CUIDADOR)
      references CUIDADOR (ID_CUIDADOR)
      on delete restrict on update restrict;

alter table SERVICIO
   add constraint FK_SERVICIO_RELATIONS_TIPO_SER foreign key (ID_TIPO_SERVICIO)
      references TIPO_SERVICIO (ID_TIPO_SERVICIO)
      on delete restrict on update restrict;

alter table SERVICIO
   add constraint FK_SERVICIO_RELATIONS_ESTADO_E foreign key (ID_ESTADO_ENTREGA)
      references ESTADO_ENTREGA (ID_ESTADO_ENTREGA)
      on delete restrict on update restrict;

alter table SERVICIO
   add constraint FK_SERVICIO_RELATIONS_MASCOTA foreign key (ID_MASCOTA)
      references MASCOTA (ID_MASCOTA)
      on delete restrict on update restrict;

